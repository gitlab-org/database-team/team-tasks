<!--
Thanks for filing this issue to help us understand the rationale behind changing this very
large table! If your change falls into one of our accepted exceptions, you don't need this issue.
If not, please provide the following!
 -->

## Information about this change:

**Table:** 
**Change:** 
**Related issue:** 
**Table size:** <!-- Check `Tale Size` under the group that owns the table in https://gitlab-com.gitlab.io/gl-infra/platform/stage-groups-index/ -->
**Change type:** 
**Product manager for the feature:** 

### Rationale for the change

<!-- Please give us a detailed reason why the change needs to happen, and what other options
were already considered and why they were ruled out. -->

### Checklist

- [ ] Updated title to "Schema change request: <table name>"
- [ ] Issue is assigned to the change author
- [ ] Change information is complete
- [ ] Rationale is complete
- [ ] `@@gitlab-org/database-team/triage` are mentioned in a comment once the above is complete

### Database Team Reviewer

- [ ] This change is approved
      _Final denial rationale: <Link to discussion or note where decision was made>_
- [ ] Add a comment (new thread) informing the requestor of the final decision
- [ ] Close the issue

/assign me
/confidential
