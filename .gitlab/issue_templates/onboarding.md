This issue contains pointers and starting points for everything database
at GitLab.

### Database reviews and maintainer training

* [ ] Familiarize yourself with our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html) and [Database Review Guidelines](https://docs.gitlab.com/ee/development/database_review.html#how-to-prepare-the-merge-request-for-a-database-review)
* [ ] Get to know the [maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#maintainer) role in code reviewing and review the process of [how to become one](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer).

### Chatops and Database Labs

* [ ] Explore [Joe Bot & Database Lab](https://docs.gitlab.com/ee/development/database/database_lab.html)
* [ ] Set yourself up for [ChatOps](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html)
* [ ] Get a query plan for `SELECT * FROM users where username='$yourname'` or any other interesting query from Joe Bot.
* [ ] In a the same Joe Bot session, try dropping the index `index_users_on_username` and repeat above experiment to see how the plan changes.
* [ ] Explore the [Postgres.ai documentation](https://postgres.ai/docs/)
* [ ] Set up [pgai gem](https://docs.gitlab.com/ee/development/database/database_lab_pgai.html) to connect to database lab easily.

### Groups, Slack and other communication

* [ ] Get yourself added to [`@gl-database`](https://gitlab.com/groups/gl-database) as a Developer. Reach out to any Maintainer of the group.
* [ ] Relevant Slack channels: `#database, #g_database, #backend, #production`
* [ ] Pro tip: Turn your [global notification settings](https://gitlab.com/-/profile/notifications) to "on mention only"
* [ ] Get yourself invited for Database Office Hours and add the [GitLab Team Meetings](https://about.gitlab.com/handbook/tools-and-tips/#gitlab-team-meetings-calendar) calendar to your calendar
* [ ] Make sure your role description in the team YAML file includes `, Database` to add you to the [team page](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/data_stores/database/) - [example MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/86185#note_623776780)

### GitLab.com database monitoring, logs

* [ ] Explore Grafana dashboards in general and around PostgreSQL, e.g. the [PostgreSQL Overview](https://dashboards.gitlab.net)
* [ ] Find GitLab.com logging in [Kibana](https://log.gitlab.net/)

### GitLab development

* [ ] Read through the [Database Guides](https://docs.gitlab.com/ee/development/database/) section, in particular [Migration Style Guide](https://docs.gitlab.com/ee/development/migration_style_guide.html), [Post Deployment Migrations](https://docs.gitlab.com/ee/development/database/post_deployment_migrations.html), [Background Migrations](https://docs.gitlab.com/ee/development/database/batched_background_migrations.html)

### Database Team

* [ ] Ask one of your teammates to add you to the [Database Team Project](https://gitlab.com/gitlab-org/database-team)
* [ ] Find links to boards, issue trackers and other documentation on
  the [Database Team Page](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/data_stores/database)
* [ ] Find the reference materials and more recommendations in [Database Engineering page](https://handbook.gitlab.com/handbook/engineering/development/database/#recommended-links-and-reference-materials)
* [ ] Familiarize yourself with our [Product Direction](https://about.gitlab.com/direction/database/)
* [ ] Send an improvement MR to this issue template (perhaps with additional points for future members joining the team)

### Access Requests
* [ ] File an access-request: [New Issue to Request Access](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=&issuable_template=Individual_Bulk_Access_Request)
  * [ ]  For the `db-lab` chef role
  * [ ]  For postgres.ai `AllFeaturesUser` role
  * [ ]  To be added to the `@db-team` slack group

### Other

* [ ]  Review info on [teleport](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/teleport/Connect_to_Database_Console_via_Teleport.md) and how to access dedicated console servers for gstg / gprd
* [ ]  Update profile to be a database reviewer && add yourself to the team page
* [ ]  Update info on which Team Meetings you should request access to and how to ask for it (ask `@People Connect Bot ` or in the `#people-connect` slack channel) - [See more](https://about.gitlab.com/handbook/people-group/people-connect/)

/confidential
