*This page may contain information related to upcoming products, features and functionality.
It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes.
Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc.*

# Capacity

List any noteworthy PTO that may impact the capacity for the planned milestone

# Boards
- [Triage Board](https://gitlab.com/groups/gitlab-org/-/boards/2305765?scope=all&utf8=%E2%9C%93&label_name[]=database%3A%3Atriage)
- [Validation Board](https://gitlab.com/groups/gitlab-org/-/boards/2305758?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Adatabase&label_name[]=database%3A%3Avalidation)
- [Build Board](https://gitlab.com/groups/gitlab-org/-/boards/1324138?&label_name[]=database%3A%3Aactive&label_name[]=group%3A%3Adatabase)

# Planning

Indicate major efforts by linking to the appropriate epic. Under each epic indicate who is focusing on these efforts and list the individual issues assigned for this milestone. 

## Infradev Issues
Infradev issues take priority. Review the links below to determine if there are any infradev issues that are not being addressed and need to be prioritized for this milestone
- [Infradev Reports](https://gitlab.com/gitlab-org/infradev-reports)
- [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&state=opened&label_name[]=group%3A%3Adatabase&label_name[]=infradev) with `group::database` and `infradev` labels

## Top Priorities for `milestone`

### [Epic Title]()

Who: 

Issues
- Issue link 1
- Issue link 2 


## Additional Issues for consideration

# Kickoff Video

/label ~"group::database" ~"devops::enablement" ~"section::enablement" ~"Category:Database" 
/assign @iroussos @alexives
